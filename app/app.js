'use strict';


import React, {Component} from 'react';
import {
	View,
	StylSheet
} from 'react-native';

import {createStore} from 'redux';
import {Provider} from 'react-redux';
import rootReducer from './reducers/rootReducer';
import Feeds from './Feed/All.js';
import Router from 'react-native-simple-router';


const store =  createStore(rootReducer);

const firstRoute = {
	name: 'Feeds',
	component: Feeds,
};

export default class App extends Component {
	render(){
		return(
	      <Provider store={store}>
	      	<Router firstRoute={firstRoute}/>
	      </Provider>
		);
	}
}