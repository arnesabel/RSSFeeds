'use strict';

export const LOAD_FEEDS = 'loadFeeds';
export const SET_FEED_FILTER = 'favoritFilter';
export const READ_FEED = 'readFeed';

export const ADD_TAG = 'addTag';
export const TAG_FILTER = 'filterTag';


export const ADD_CHANNEL = 'addChannel';
export const EDIT_CHANNEL = 'editChannel';
export const CHANNEL_FILTER = 'channelFilter';
