'use strict';

import {combineReducers} from 'redux';
import tag, * as tagReducer from './tagReducer.js';
import channel from './channelReducer.js';
import feed from './feedReducer.js';

//Los sub states van a ser nombrados con el nombre del reducer anadido.
//Ejemplo state { reducer1:{}, reducer2:{}}, cada uno conforma un nested object del state global.

export default combineReducers({
	tag,
	channel,
	feed,

})

export const getTags = (state) => {	
	return tagReducer.getTags(state.tag);
}