'use strict';

import * as Actions from '../actionsTypes.js';

const DEFAULT_STATE = {
	channels: [{
		name: 'Facebook',
		url: 'www.facebook.com',
		imgpath: 'path',
	}],
	filter: '',
};

export default function(state = DEFAULT_STATE, action){
	switch(action.type){
		case Actions.ADD_CHANNEL:
			return { channels: [...state.channels, action.payload], //Validar que no tenga la misma direccion que otro.
					 filter: state.filter,
					};
		case Actions.EDIT_CHANNEL:
			let c = state.channels[action.index];
			return { channels: [ ...state.channels.splice(0,index),
					edit(c, action.name),
					...state.channels.splice(index),
					  ],
					  filter: state.filter,
		};
		case Actions.VIEW_CHANNEL:
			return {
					channels: state.channels,
					filter: action.index,
			};
		default:
			return state;
	}
};

const edit = (channel, name) => {
	let c = {...channel};
	c.description = name;
	return c;
}

