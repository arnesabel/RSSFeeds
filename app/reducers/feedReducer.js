'use strict';

import * as Actions from '../actionsTypes.js';
import * as CONST from '../common.js';
//Reducer de los Feeds

const DEFAULT_STATE = {
	feedFilter: CONST.FEED_FILTERS.ALL,
	feeds: [],
};

export default function (state = DEFAULT_STATE, action){
	switch(action.type){
		case Actions.SET_FEED_FILTER: 
			return {
				feedFilter: action.filter,
				feeds: state.feeds,
			}
		case Actions.READ_FEED:
			let f = state.feeds[action.index];
			return {
				feedFilter: state.feedFilter,
				feeds: [
						...state.feeds.splice(0, index),
						edit(f),
						...state.feeds.splice(index),
				],
			};
		case Actions.LOAD_FEEDS:
			return {
				feedFilter: state.feedFilter,
				feeds: [...state.feeds,
						action.payload,
				],
			};
		default: 
			return state;
	}

}

const edit = (feed)=>{
	return {
		...feed,
		read: true,
	};
};

