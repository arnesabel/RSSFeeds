'use strict';

import * as Actions from '../actionsTypes.js';

const DEFAULT_STATE = { tags:[],
						filter:'',
						};

export default function (state = DEFAULT_STATE, action){
	switch(action.type){
		case Actions.ADD_TAG:
			let tags = state.tags;
			return {
					tags: [ ...tags,
							{description:action.description}
							],
					filter: state.filter,
			};
		default: 
			return state;
	}
	return state;
};

//Selector, del estado completo solo quiero los tags. 
export const getTags = (tags) => tags;