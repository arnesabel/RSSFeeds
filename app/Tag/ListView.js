'use strict';
import React, {Component} from 'react';
import {
	View,
	TouchableHighlight,
	StyleSheet,
	Text,
	ListView,
} from 'react-native';
import tag from './tag.js';

export default class CustomListView extends Component {


	componentWillMount() {
		const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
					this.setState({
				ds: ds.cloneWithRows(this.props.tags),
			});
		// setInterval(()=>{
		// 	this.setState({
		// 		ds: ds.cloneWithRows(this.props.tags),
		// 	}); 
		// }, 5000); Para probar que, el prop tiene los tags nuevos pero como el listview no lo recibe, no actualiza este ya que su ds proviene del state.

	}

	componentWillReceiveProps(nextProps) {
	    const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
		this.setState({
			ds: ds.cloneWithRows(nextProps.tags), //no es un objeto plain de javascript por lo que no puede estar en el store
		});
	}

	renderSingle(tag){
		return (
			<View>
				<Text>{tag.description}</Text>
			</View>
			);
	}
	render(){
		return (
			<ListView dataSource={this.state.ds} 
			renderRow={this.renderSingle}/>
		);
	}
}
