'use strict';

import * as Actions from '../actionsTypes.js';

export const addTagAction = (text)=>{
	return {
		type: Actions.ADD_TAG,
		description: text,
	};
};