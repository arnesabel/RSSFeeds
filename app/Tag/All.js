'use strict';

import React, {Component} from 'react';
import {
	View, 
	TouchableHighlight,
	Button,
	StyleSheet,
	TextInput,
	Modal,
} from 'react-native';

import ListView from './ListView.js';
import { getTags } from '../reducers/rootReducer.js';
import {connect} from 'react-redux';
import { addTagAction } from './actions.js';

//Props que se van a inyectar al elemento.
const mapStateToProps = (state) =>{
	return ({
		tags: getTags(state),
	});

};

const mapDispatchToProps = (dispatch)=>{
	return ({
		onClick: (text) => {
			dispatch(addTagAction(text));
		}
	});
}


class All extends Component{

	componentWillMount() {
		this.setState({modalVisible:false,
					   text: '',
		});
	}

	showModal(){
			this.setState({modalVisible: !this.state.modalVisible});
	}

	render(){
		return(
			<View>
				<Button  title="Agregar" color="#841584" onPress={this.showModal.bind(this)}/>
				<ListView tags={this.props.tags}/>
			        <Modal
			          animationType={"slide"}
			          transparent={true}
			          visible={this.state.modalVisible}
			          onRequestClose={() => {alert("Modal has been closed.")}}
			          >
			         <View style={{justifyContent:'center', alignItems:'center', backgroundColor:'white',
			     					height:250,
			     				}}>

			          	<TextInput style={{height: 40, borderColor: 'gray', borderWidth: 1}}
			          		onChangeText={(text)=>(this.setState({text:text}))}/>

      					<Button  title="Guardar" color="#841584" onPress={()=>{
      							if(this.state.text !== '')
      								this.props.onClick(this.state.text);
      					}}/>
      					<Button  title="Cancelar" color="#841584" 
      						onPress={()=>{this.setState({modalVisible:!this.state.modalVisible})}}/>
			         </View>
			        </Modal>
			      
			</View>

		);
	}

}

export default connect(mapStateToProps, mapDispatchToProps)(All);