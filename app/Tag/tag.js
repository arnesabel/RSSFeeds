'use strict';

import React, {Component} from 'react';
import {View,
		Text,
		StyleSheet,
} from 'react-native';

//Stateless Component
const tag = ({description}) => {
	return(
		<Text style={{color:'black', fontSize:20}}>{description}</Text>
		);
};

export default tag;