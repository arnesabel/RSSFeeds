'use strict';
import React, {Component} from 'react';
import {
	View,
	TouchableHighlight,
	StyleSheet,
	Text,
	ListView,
} from 'react-native';
import Channel from './channel.js';

export default class CustomListView extends Component {
	componentWillMount() {
		const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
					this.setState({
				ds: ds.cloneWithRows(this.props.channels),
			});
	}

	componentWillReceiveProps(nextProps) {
	    const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
		this.setState({
			ds: ds.cloneWithRows(nextProps.channels), 
		});
	}

	renderSingle(channel){
		return (
			<TouchableHighlight onLongPress={this.props.onEditClick(channel)}>
				<View>
					<Channel name={channel.name}/>
				</View>
			</TouchableHighlight>
			);
	}
	render(){
		return (
			<ListView dataSource={this.state.ds} 
			renderRow={this.renderSingle.bind(this)}/>
		);
	}
}
