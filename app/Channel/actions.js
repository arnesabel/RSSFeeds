'use strict';

import * as Actions from '../actionsTypes.js';

export const addChannel = (channel)=>{
	return (
		{
			type: Actions.ADD_CHANNEL,
			payload: channel,
		}
	);
};

export const editChannel = (index, name)=>{
	return(
		{
			type: Actions.EDIT_CHANNEL,
			index: index,
			name: name,
		}
	);
};

export const filterChannels = (index) => {
	return(
	{	
			type: Actions.CHANNEL_FILTER,
			index: index,
	});
}