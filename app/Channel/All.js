'use strict';

import React, {Component}  from 'react';
import {
	View,
	StyleSheet,
	Modal,		
	TextInput,
	Button,
} from 'react-native';
import { connect } from 'react-redux';

//Custom obj
import ListView from './ListView.js';
import { addChannel, editChannel } from './actions.js';


const mapStateToProps = (state) => {
	return ({
		channels: state.channel.channels,
	}
	);
} 

//Tienen el objeto dispatch, por lo que tiene un closure, ya que el objeto dispatch sobrevive la llamada
//de la funcion
const mapDispatchToProps = (dispatch) => {
	return ({
		onAddClick: (channel) =>{
			dispatch(addChannel(channel));
		},
		onEditClick: (index, name) =>{
			dispatch(editChannel(index, name));
		},
	});
}

class All extends Component{
	
	constructor(props){
		super(props);
		this.state = {
			modalVisible: false,
		};	

		this.channel = undefined; //aux for editing channels

	}

	addChannel(){
		let channel = {
			name:	this.state.name,
			link:	this.state.link,
		}
		//this.props.onAddClick(channel);
		this.setState({modalVisible: !this.state.modalVisible});
	}

	showModal(){
		this.setState({
			modalVisible: true,
		})
	}

	showEditModal(channel){
		this.channel = channel;
		this.setState({
			modalVisible: true,
		})
		console.log(channel);
	}

	editChannel(index, channel){

	}

	render(){
		return(
			<View> 
				<Button title="Agregar" onPress={this.showModal.bind(this)}/>
				<ListView channels={this.props.channels}
				 onEditClick={this.showEditModal.bind(this)}/>

				 <Modal visible={this.state.modalVisible}
			 	  animationType={"slide"}
		          transparent={true}
		          visible={this.state.modalVisible}
		          onRequestClose={() => {alert("Modal has been closed.")}}
		          >
			        <View style={{justifyContent:'center', 
			        			alignItems:'center', 
			        			backgroundColor:'white',
			     				height:250,
			     				}}>
			          	<TextInput style={{height: 40, borderColor: 'gray', borderWidth: 1}}
		          		onChangeText={(text)=>(this.setState({text:text}))}/>

				 		<Button title="Guardar" onPress={this.addChannel.bind(this)}/>
				 	</View>
				 </Modal>
			</View>);
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(All);