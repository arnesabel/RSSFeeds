'use strict';

import React, {Component} from 'react';
import {
	View,
	Text,
	StyleSheet,
	Image,
} from 'react-native';

//Stateless component
const channel = ({name, imgpath = ''}) => {

	return (
		<View style={styles.container}>
			<Image style={styles.image} src={imgpath}/>
			<Text>{name}</Text>
		</View>
	);
};

const styles = {
	container: {
		flex: 1,
		borderWidth: 1,
		borderColor: 'skyblue',
		borderStyle: 'solid',
	},
	image:{
		width: 200, 
		height: 100,
	},
}

export default channel;