'use strict';

import React, {Component}  from 'react';
import {
	View,
	StyleSheet,
	Modal,		
	TextInput,
	Button,
} from 'react-native';
import { connect } from 'react-redux';

//Custom obj
import ListView from './ListView.js';
import { viewFeed, setFeedFilter } from './actions.js';
import FilterTab from './FilterTab.js';



const mapStateToProps = (state) => {
	console.log(state.feed.feedFilter);
	return ({
		feeds: state.feed.feeds, //Filtro de los feeds
		feedFilter: state.feed.feedFilter,
	});
} 


const mapDispatchToProps = (dispatch) => {
	return ({
		viewFeed: (feed) =>{
			dispatch(viewFeed(feed));
		},
		showAll: (filter)=>{
			dispatch(setFeedFilter(filter));
		},
		showFavorites: (filter)=>{
			dispatch(setFeedFilter(filter));
		}
	});
}

				// <ListView channels={this.props.feeds}
				//  onClick={this.viewFeed.bind(this)}/>

class All extends Component{
	
	viewFeed(feed){
		this.props.viewFeed(feed);
		//Abrir feed en otro route
    }
	render(){
		return(
			<View> 
				<FilterTab activeFilter={this.props.feedFilter} showAll={this.props.showAll}
				showFavorites={this.props.showFavorites}/>
			</View>
		);
	}
};

export default connect(mapStateToProps, mapDispatchToProps)(All);