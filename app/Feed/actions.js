'use strict';

import * as Actions from '../actionsTypes.js';
import * as CONST from '../common.js';

export const readFeed = (feed) =>{
	return(
	{
		type: Actions.READ_FEED,
		payload: feed,
	})
}

export const loadFeeds = (feeds)=>{
	return({
		type: Actions.LOAD_FEEDS,
		payload: feeds,
	});
}

export const setFeedFilter = (filter)=>{
	return({
		type: Actions.SET_FEED_FILTER,
		filter: filter,
	});
}
