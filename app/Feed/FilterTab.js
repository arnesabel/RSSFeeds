'use strict';

import React, {Component} from 'react';
import {
	View,
	StyleSheet,
	TouchableHighlight,
} from 'react-native';

import TabButton from './TabButton.js';
import * as CONST from '../common.js';

const FilterTab = ({activeFilter, showAll, showFavorites}) =>{


			return (
		      <View style={styles.container}>
	        	<TabButton text={CONST.FEED_FILTERS.ALL} activeFilter={activeFilter} 
	        	onClick={ ()=> { showAll(CONST.FEED_FILTERS.ALL)} }/>
				
	        	<TabButton text={CONST.FEED_FILTERS.FAVORITES} activeFilter={activeFilter} 
	        	onClick={ ()=>{ showFavorites(CONST.FEED_FILTERS.FAVORITES)}}/>

		      </View>
		);
}


const styles = StyleSheet.create({
	container:{
    	flexDirection: 'row',
	},
});

export default FilterTab;
