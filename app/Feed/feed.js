'use strict';


import React, {Component} from 'react';
import {
	View,
	Text,
	StyleSheet,
} from 'react-native';
/*
	Stateless Component puesto que solo muestra informacion y ejecuta una funcion pasada por otro elemento.
	Es solo una funcion que muestra un texto
*/
const feed = ({text}) => {
	return (
			<View style={styles.container}>
				<Text style={styles.text}>{text}</Text>
			</View>
		);
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		borderWidth: 1,
		borderStyle: 'solid',
		borderColor: 'skyblue',
	},
	text: {
		fontSize: 20,
		color: 'blue',
	},
});

export default feed;