'use strict';
import React, {Component} from 'react';
import {
	View,
	TouchableHighlight,
	StyleSheet,
	Text,
	ListView,
} from 'react-native';
import Feed from './feed.js';

export default class CustomListView extends Component {
	componentWillMount() {
		const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
					this.setState({
				ds: ds.cloneWithRows(this.props.feeds),
			});
	}

	componentWillReceiveProps(nextProps) {
	    const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
		this.setState({
			ds: ds.cloneWithRows(nextProps.channels), 
		});
	}

	renderSingle(feed){
		return (
			<TouchableHighlight onClick={this.props.onClick(feed)}>
				<View>
					<Feed name={feed.name}/>
				</View>
			</TouchableHighlight>
			);
	}
	render(){
		return (
			<ListView dataSource={this.state.ds} 
			renderRow={this.renderSingle.bind(this)}/>
		);
	}
}
