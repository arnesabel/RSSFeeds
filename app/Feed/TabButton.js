'use strict';

import React, {Component} from 'react';
import {
	View,
	Text,
	StyleSheet,
	TouchableHighlight,
} from 'react-native';

const TabButton = ({text, activeFilter, onClick}) => {

		let container;
		let texts;
		if(text === activeFilter){
			return(
				<View style={pressed.container}>
					<Text style={pressed.text}>{text}</Text>
				</View>
			);
		} else {		
			return (
	      		<TouchableHighlight onPress={onClick}>
	      			<View style={unpressed.container}>
						<Text style={unpressed.text}>{text}</Text>
					</View>
	      		</TouchableHighlight>
			);
		}
	
};

const unpressed = StyleSheet.create({
	container:{
		borderWidth: 1,
		borderRadius: 5,
		borderColor: 'gray',
		justifyContent: 'center',
		alignItems: 'center',
		width: 150,
		height: 40,
		padding: 10,
	},
	text:{
		color: 'gray',
		fontSize: 12,
		fontWeight: 'bold',
	},
});

const pressed = StyleSheet.create({
	container:{
		borderWidth: 1,
		borderRadius: 5,
		borderColor: 'skyblue',
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: 'skyblue',
		width: 150,
		height: 40,
		padding: 10,
	},
	text:{
		color: 'white',
		fontSize: 16,
		fontWeight: 'bold',
	},
});

export default TabButton;